var platforms = [];

var speed = 2;
var text_pause ;
var text_restart;
var text_life;
var text_floor;
var status = 'play';
var keyboard;
var nailsound;
var jumpsound;
var stepsound;
var BGMsound;
var lastsound;
var starsound;
var weapon;
var tween = undefined;
var hurt = false;
var load = false;
var p;
var star = false;

var playState = {
    preload: function() {
        
    },
    create: function() {
        game.global.life = 10;
        game.global.floor = 1; 
        BGMsound =game.add.audio('BGM');
        nailsound = game.add.audio('nail');
        jumpsound = game.add.audio('jump');
        stepsound = game.add.audio('step');
        starsound = game.add.audio('star');
        BGMsound.play('',0,1,true);
        //Keyboard Input
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'space': Phaser.Keyboard.SPACEBAR
        });
        //Create L-Wall
        leftWall = game.add.sprite(0, 0, 'wall');
        leftWall.scale.setTo(1,1.5);
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;
        //Create R-wall
        rightWall = game.add.sprite(game.width-17, 0, 'wall');
        rightWall.scale.setTo(1,1.5);
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;
        //Create ceiling
        ceiling = game.add.image(0, 0, 'ceiling');
        ceiling.scale.setTo(1.5,1);
        //Create Player
        player = game.add.sprite(game.width/2, 40, 'player');
        player.anchor.setTo(0.5,0.5);
        player.direction = 10;
        game.physics.arcade.enable(player);
        player.body.gravity.y = 500;
        player.animations.add('left', [0, 1, 2, 3], 8);
        player.animations.add('right', [9, 10, 11, 12], 8);
        player.animations.add('flyleft', [18, 19, 20, 21], 12);
        player.animations.add('flyright', [27, 28, 29, 30], 12);
        player.animations.add('fly', [36, 37, 38, 39], 12);
        player.unbeatableTime = 0;
        player.touchOn = undefined;
        //tween
        tween = game.add.tween(player).to({ alpha: 1 }, 50, Phaser.Easing.Linear.None, true, 0, 1000, true);
        //tween.pause();
        //tween.resume();
        //Create Fireball
        //  Creates 30 bullets, using the 'bullet' graphic
        weapon = game.add.weapon(30, 'fireball', 5);
        weapon.bullets.forEach((b) => {
            b.scale.setTo(0.1, 0.1);
            b.body.updateBounds();
        }, this);
        //  The bullet will be automatically killed when it leaves the world bounds
        weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        //  Because our bullet is drawn facing up, we need to offset its rotation:
        weapon.bulletAngleOffset = 270;
        //  The speed at which the bullet is fired
        weapon.bulletSpeed = 400;
        //  Speed-up the rate of fire, allowing them to shoot 1 bullet every 60ms
        weapon.fireRate = 200;
        //change fire angle
        weapon.fireAngle = Phaser.ANGLE_DOWN;

        game.physics.arcade.enable(weapon);
        weapon.trackSprite(player, 0, 0);

        /// Particle
        this.emitter = game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;

        //Create text
        text_life = game.add.text(15, 5, 'Life : ' + game.global.life, {fill: '#ff0000', fontSize: '20px'});
        text_floor = game.add.text(game.width-50, 5, "B" + game.global.floor, {fill: '#ff0000', fontSize: '20px'});
        
        text_restart = game.add.text(game.width/2, game.height/2+20, "Press Enter to Restart", {fill: '#000000', fontSize: '10px'});
        text_restart.anchor.setTo(0.5,0.5);
        text_restart.visible = false;
        text_restart.alpha = 0;
        game.add.tween(text_restart).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        text_pause = game.add.text(game.width/2, game.height/2, "Pause", {fill: '#ff0000', fontSize: '20px', align: 'center'});
        text_pause.anchor.setTo(0.5, 0.5);
        text_pause.visible = false;
    },
    
    createPlatforms: function() {
        if(game.time.now-game.global.prev > 600 && status == 'play'){
            game.global.prev = game.time.now;
            this.create1platform();
            game.global.floor=game.global.floor+1;
        }
    },
    create1platform: function() {
        var platform;
        var x = Math.random()*(game.width - 96 - 40) + 20;
        var y = game.height;
        var rand = Math.random() * 100;

        if(rand < 20) {
            platform = game.add.sprite(x, y, 'normal');
        } else if (rand < 40) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 60) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 80) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }

        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platforms.push(platform);

        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },
    effect: function(player, platform) {
        if(platform.key == 'conveyorRight' && player.body.touching.down) {
            player.body.x += 2;
        }
        if(platform.key == 'conveyorLeft' && player.body.touching.down) {
            player.body.x -= 2;
        }
        if(platform.key == 'trampoline' && player.body.touching.down) {
            jumpsound.play();
            platform.animations.play('jump');
            player.body.velocity.y = -350-(speed-2);
        }
        if(platform.key == 'nails') {
            if(!tween.isPaused && hurt){
                console.log(tween.isPaused);
            }
            else if (player.touchOn !== platform && player.body.touching.down) {
                nailsound.play();
                game.global.life -= 1;
                player.touchOn = platform;
                game.camera.flash(0xff0000, 100);
                
            }
        }
        if(platform.key == 'normal') {
            if (player.touchOn !== platform && player.body.touching.down) {
                stepsound.play();
                if(game.global.life < 10) {
                    game.global.life += 1;
                }
                player.touchOn = platform;
            }
        }
        if(platform.key == 'fake') {
            if(player.touchOn !== platform && player.body.touching.down) {
                platform.animations.play('turn');
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                player.touchOn = platform;
            }
        }
    },
    unpause: function(){
        
        if(game.paused){
            if(keyboard.space.isDown){
                
                game.paused = false;
                text_pause.visible = false;
                if(star){
                    starsound.resume();
                }
                else{
                    BGMsound.resume();
                    BGMsound._sound.playbackRate.value = lastsound;
                }
                
            }
        }
    },
    hit: function(weapon,platform) {
        console.log("hit");
        this.emitter.emitX = platform.x;
        this.emitter.emitY = platform.y;
        this.emitter.start(true, 800, null, 15); 
        platform.destroy();
        weapon.kill();
    },
    collide: function(player, capsule){
        capsule.kill();
        BGMsound.pause();
        starsound.play();
        player.alpha = 0;
        star = true;
        var tween2 = game.add.tween(player).to({ alpha: 1 }, 50, Phaser.Easing.Linear.None, true, 0, 1000, true);
        player.body.gravity.y = 100;
        setTimeout(function(){
            player.body.gravity.y = 500; 
            tween2.pause(); 
            starsound.stop();
            BGMsound.resume();
            BGMsound._sound.playbackRate.value = lastsound;
            star = false;
        }, 5000);
        console.log(capsule);
    },
    update: function() {
        //pause
        
        if(game.paused == false){
            if(keyboard.space.isDown){
                BGMsound.pause();
                starsound.pause();
                text_pause.visible = true;
                //z-index on the top
                game.world.bringToTop(text_pause);
                game.paused = true;
                keyboard.space.onDown.add(this.unpause, self);
            }
            
        }
        
        /*else if(game.paused == true){
            
            if(keyboard.space.isDown){
                BGMsound.resume();
                BGMsound._sound.playbackRate.value = lastsound;
                game.paused = false;
            }
        }*/
        //check situation
        /*if(status == 'gameover' && keyboard.enter.isDown){
            
            game.global.floor = 1;
            player = game.add.sprite(game.width/2, 40, 'player');
            player.anchor.setTo(0.5,0.5);
            player.direction = 10;
            game.physics.arcade.enable(player);
            player.body.gravity.y = 500;
            player.animations.add('left', [0, 1, 2, 3], 8);
            player.animations.add('right', [9, 10, 11, 12], 8);
            player.animations.add('flyleft', [18, 19, 20, 21], 12);
            player.animations.add('flyright', [27, 28, 29, 30], 12);
            player.animations.add('fly', [36, 37, 38, 39], 12);
            player.unbeatableTime = 0;
            player.touchOn = undefined;
            status = "play";
        }*/
        // check platform wall collisions
        this.physics.arcade.collide(player, platforms, this.effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        
        this.physics.arcade.overlap(weapon.bullets, platforms, this.hit, null, this);
        this.physics.arcade.overlap(player, p, this.collide);
        //speed and rate of weapon
        //weapon.bulletSpeed += speed;
        //weapon.fireRate += speed;
        //check ceiling collision
        if(player.body.y < 0) {
            player.body.y += 10;
            if(player.body.velocity.y < 0) {
                player.body.velocity.y = 0;
            }
            if(game.time.now > player.unbeatableTime) {
                nailsound.play();
                game.global.life -= 2;
                game.camera.flash(0xff0000, 100);
                player.unbeatableTime = game.time.now + 2000;
                hurt = true;
                player.alpha = 0;
                tween = game.add.tween(player).to({ alpha: 1 }, 50, Phaser.Easing.Linear.None, true, 0, 1000, true);
                //tween.resume();
                setTimeout(function(){tween.pause()},2000);
            }
            /*else{
                hurt = false;  
                tween.pause();
                //player.alpha = 1;
            }*/
        }
        //checkGameOver
        if(game.global.life<0 || player.y>game.height){
            
            platforms.forEach(function(s) {s.destroy()});
            platforms = [];
            BGMsound.stop();
            starsound.stop();
            tween = undefined;
            load = false;
            star = false;
            hurt = false;
            speed = 2;
            if(p!=undefined){
                p.kill();
            }
            game.state.start("gameover");
        }
        //updatePlayer
        if(keyboard.left.isDown) {
            player.body.velocity.x = -250;
        } else if(keyboard.right.isDown) {
            player.body.velocity.x = 250;
        } else {
            player.body.velocity.x = 0;
        }
        var vx = player.body.velocity.x;
        var vy = player.body.velocity.y;

        if (vx < 0 && vy > 0) {
            player.animations.play('flyleft');
        }
        if (vx > 0 && vy > 0) {
            player.animations.play('flyright');
        }
        if (vx < 0 && vy == 0) {
            player.animations.play('left');
        }
        if (vx > 0 && vy == 0) {
            player.animations.play('right');
        }
        if (vx == 0 && vy != 0) {
            player.animations.play('fly');
        }
        if (vx == 0 && vy == 0) {
            player.frame = 8;
        }
        //updatePlatforms
        for(var i=0; i<platforms.length; i++) {
            var platform = platforms[i];
            platform.body.position.y -= speed;
            if(star)platform.body.position.y += speed-1;
            if(platform.body.position.y <= -20) {
                platform.destroy();
                platforms.splice(i, 1);
            }
        }
        if(p != undefined){
            p.y -= speed;
            if(p.y<=0){
                p.destroy();
            }
        }
        //accelerate
        if(game.global.floor % 20 == 0 && game.global.floor <= 100){
            speed += 0.01;
            
            BGMsound._sound.playbackRate.value+=0.001;
            lastsound = BGMsound._sound.playbackRate.value;
            
        }
        
        //updateTextsBoard
        text_life.setText('Life : ' + game.global.life);
        text_floor.setText('B' + game.global.floor);
        //fire
        if(keyboard.enter.isDown && !player.body.touching.down){
            weapon.fire();
            
        }

        if(game.global.floor % 20 == 0 && !load){
            //console.log(global.floor);
            var px = Math.random()*(game.width - 96 - 40) + 20;
            
            p = game.add.sprite(px,game.height,'capsule');
            p.scale.setTo(0.1,0.1);
            p.anchor.setTo(0.5,0.5);
            game.physics.arcade.enable(p);
            p.body.immovable = true;
            load = true;
        }
        if(game.global.floor % 20 != 0){
            load = false;
        }

        this.createPlatforms();
    },
}