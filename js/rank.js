var optionCount;
var id = 0;
function addMenuOption(text,name,score){
    var optionStyle = { font: '30pt TheMinion', fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
    
    if(text == 1){
        optionStyle = { font: '40pt TheMinion', fill: 'red', align: 'left', stroke: 'rgba(,0,0,0)', srokeThickness: 10};
    }
    var txt = game.add.text(30, game.height/6*(this.optionCount+1), text, optionStyle);
    txt.stroke = "rgba(0,0,0,0";
    txt.strokeThickness = 4;
    game.add.text(game.width/6*4,game.height/6*(this.optionCount+1), score, optionStyle);
        
    game.add.text(game.width/5, game.height/6*(this.optionCount+1), name, optionStyle);
    optionCount ++;
}
var rankState = {
    addMenuOption: function(text,name,score) {
        var optionStyle = { font: '30pt TheMinion', fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
        var txt = game.add.text(30, game.height/6*(this.optionCount+1), text, optionStyle);
        txt.stroke = "rgba(0,0,0,0";
        txt.strokeThickness = 4;
        game.add.text(game.width/5*2,game.height/6*(this.optionCount+1), score, optionStyle);
        
        game.add.text(game.width/50, game.height/6*(this.optionCount+1), name, optionStyle);
        this.optionCount ++;
    },
    preload: function(){
        //game.load.image('progressBar','assets/progressBar.png');
    },
    create: function(){
        var nameLabel2 = game.add.text(game.width/2, 80, 'Ranking', { font: '50px Arial', fill: '#ffffff' }); 
        nameLabel2.anchor.setTo(0.5, 0.5);
        nameLabel2.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        optionCount = 1;
        
        //get ranklist
        var listRef = firebase.database().ref("ranklist").orderByChild("Score").limitToFirst(3);
        id = 0;
        listRef.once('value')
            .then(function(snapshot) {
                snapshot.forEach(function(childSnapshot){
                    id++;
                    var childData=childSnapshot.val();
                    addMenuOption(id, childData.Name, "B"+(1000000-childData.Score));
                })
            })

        //Key event
        var EscKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        EscKey.onDown.add(this.start, this);  

        var leave = game.add.text(game.width/2, game.height-25, 'Press Esc to leave', { font: '50px Arial', fill: '#ffffff' }); 
        leave.anchor.setTo(0.5, 0.5);
        leave.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        //text fade
        leave.alpha = 0;
        game.add.tween(leave).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    },
    start: function() {
        game.state.start('menu');
    }
}
