var loadState = {
    preload: function() {
        var loadingLabel = game.add.text(game.width/2, game.height/2-25, 'loading...', { font: '30px Arial', fill: '#ffffff' }); 
        loadingLabel.anchor.setTo(0.5, 0.5);
        var progressBar = game.add.sprite(game.width/2, game.height/2+25, 'progressBar'); 
        progressBar.anchor.setTo(0.5, 0.5); 
        game.load.setPreloadSprite(progressBar);

        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails', 'assets/nails.png');
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.image('wall', 'assets/wall.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('fireball', 'assets/fire.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('background', 'assets/BG.jpg');
        game.load.image('capsule', 'assets/capsule.jpg');

        game.load.audio('BGM', 'music/BGM.wav');
        game.load.audio('nail', 'music/nail.wav');
        game.load.audio('jump', 'music/jump.wav');
        game.load.audio('step', 'music/step.wav');
        game.load.audio('star', 'music/star.wav');
        
    },
    create: function() {
        game.state.start('menu');
    }
}